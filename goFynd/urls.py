"""goFynd URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
import project.views

urlpatterns = [
    #path('admin/', admin.site.urls),
    path('login/', project.views.login),
    path('createadmin/', project.views.createadmin),
    path('createclient/', project.views.createclient),
    path('creategenre/', project.views.creategenre),
    path('logout/', project.views.logout),
    path('viewgenre/', project.views.viewgenre),
    path('editgenre/', project.views.editgenre),
    path('createmovie/', project.views.createmovie),
    path('createmoviebulk/', project.views.createmoviebulk),
    path('editmovie/', project.views.editmovie),
    path('commentmovie/', project.views.commentmovie),
    path('viewmovie/', project.views.viewmovie),
    path('moviedetails/', project.views.moviedetails),
    path('viewfilters/', project.views.viewfilters),
    path('search/', project.views.search),
    path('uniqueusername/', project.views.uniqueusername),
    path('uniquecreation/', project.views.uniquecreation),
]
