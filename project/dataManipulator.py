# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
from .models import sessionMaster, loginMaster, adminMaster, clientMaster, genreMaster, movieMaster, commentMaster
from .dataViewHolder import getLoginObject, getUserIdFromLoginInfo, getMovieObject, getClientObject
from .utility import loggerHandling
from .dataClasses import loginReuest
from datetime import datetime
logger = loggerHandling('logs/project.log')

##############################################################
##############################################################
##############################################################
################FOR EVALUATION PURPOSE ONLY###################
#####################Author:HARSHVARDHAN######################
###################DATA MANIPULATION LAYER####################
##############################################################
##############################################################
##############################################################

def insertSessionInformation(sessionInfoObject):
    try:
        sessionInsertion = sessionMaster()
        sessionInsertion.userId = getLoginObject(sessionInfoObject.userId)
        sessionInsertion.basicAuthenticate = sessionInfoObject.basicAuth
        sessionInsertion.createdOn = sessionInfoObject.createdOn
        sessionInsertion.isActive = sessionInfoObject.isActive
        sessionInsertion.sessionInfo = sessionInfoObject.sessionKey
        sessionInsertion.typeId = sessionInfoObject.typeId
        sessionInsertion.userType = sessionInfoObject.userType
        sessionInsertion.save()
        return True
    except Exception as e:
        logger.exception(e)
        return False

def insertAdminLoginDetails(adminCreationInfo):
    try:
        loginInsertion = loginMaster()
        loginInsertion.userName = adminCreationInfo.userName
        loginInsertion.password = adminCreationInfo.password
        loginInsertion.userType = "ADM"
        loginInsertion.isActive = 1
        loginInsertion.save()
        loginInfo = loginReuest(adminCreationInfo.userName, adminCreationInfo.password, "ADM")
        userId = getUserIdFromLoginInfo(loginInfo)
        return userId
    except Exception as e:
        logger.exception(e)
        return False

def insertAdminNewPersonalDetails(adminCreationInfo):
    try:
        adminMasterInfo = adminMaster()
        adminMasterInfo.userId = getLoginObject(adminCreationInfo.userId)
        adminMasterInfo.adminName = adminCreationInfo.adminName
        adminMasterInfo.number = adminCreationInfo.number
        adminMasterInfo.email = adminCreationInfo.email
        adminMasterInfo.isActive = 1
        adminMasterInfo.save()
        return True
    except Exception as e:
        logger.exception(e)
        return False

def insertClientLoginDetails(clientCreationInfo):
    try:
        loginInsertion = loginMaster()
        loginInsertion.userName = clientCreationInfo.userName
        loginInsertion.password = clientCreationInfo.password
        loginInsertion.userType = "CLIENT"
        loginInsertion.isActive = 1
        loginInsertion.save()
        loginInfo = loginReuest(clientCreationInfo.userName, clientCreationInfo.password, "CLIENT")
        userId = getUserIdFromLoginInfo(loginInfo)
        return userId
    except Exception as e:
        logger.exception(e)
        return False

def insertClientNewPersonalDetails(clientCreationInfo):
    try:
        clientMasterInfo = clientMaster()
        clientMasterInfo.userId = getLoginObject(clientCreationInfo.userId)
        clientMasterInfo.name = clientCreationInfo.adminName
        clientMasterInfo.number = clientCreationInfo.number
        clientMasterInfo.email = clientCreationInfo.email
        clientMasterInfo.isActive = 1
        clientMasterInfo.createOn = datetime.now()
        clientMasterInfo.save()
        return True
    except Exception as e:
        logger.exception(e)
        return False

def insertNewGenre(genreClassObject):
    try:
        genreMasterInfo = genreMaster()
        genreMasterInfo.genreName = genreClassObject.name
        genreMasterInfo.isActive = genreClassObject.isActive
        genreMasterInfo.save()
        return True
    except Exception as e:
        logger.exception(e)
        return False

def insertNewMovieInformation(movieClassObject):
    try:
        movieInsertInfo = movieMaster()
        movieInsertInfo.name = movieClassObject.name
        movieInsertInfo.director = movieClassObject.director
        movieInsertInfo.genre = movieClassObject.genre
        movieInsertInfo.poplularity = movieClassObject.poplularity
        movieInsertInfo.imdb = movieClassObject.imdb
        movieInsertInfo.createdOn = movieClassObject.createdOn
        movieInsertInfo.save()
        return True
    except Exception as e:
        logger.exception(e)
        return False

def deActivateANewSession(sessionObject):
    try:
        sessionActiveInfo = sessionMaster.objects.get(basicAuthenticate__exact=sessionObject.basicAuth)
        sessionActiveInfo.isActive = 0
        sessionActiveInfo.save()
        return True
    except Exception as e:
        logger.exception(e)
        return False

def updateTheValuesOfGenre(genreClassObject):
    try:
        genreInfo = genreMaster.objects.get(genreId=genreClassObject.genreId)
        genreInfo.genreName = genreClassObject.name
        genreInfo.isActive = genreClassObject.isActive
        genreInfo.save()
        return True
    except Exception as e:
        logger.exception(e)
        return False

def editingMovieInformation(movieClassObject):
    try:
        movieInfo = movieMaster.objects.get(movieId=movieClassObject.id)
        movieInfo.name = movieClassObject.name
        movieInfo.director = movieClassObject.director
        movieInfo.genre = movieClassObject.genre
        movieInfo.poplularity = movieClassObject.poplularity
        movieInfo.imdb = movieClassObject.imdb
        movieInfo.save()
        return True
    except Exception as e:
        logger.exception(e)
        return False

def insertNewComment(commentClassObject):
    try:
        commentInsertion = commentMaster()
        commentInsertion.movieId = getMovieObject(commentClassObject.movieId)
        commentInsertion.commentBy = getClientObject(commentClassObject.clientId)
        commentInsertion.commentOn = commentClassObject.createdOn
        commentInsertion.commentValue = commentClassObject.comment
        commentInsertion.save()
        return True
    except Exception as e:
        logger.exception(e)
        return False

def deletNewLoginInfo(userId):
    try:
        loginInsertion = loginMaster.objects.get(userId=userId)
        loginInsertion.delete()
        return True
    except Exception as e:
        logger.exception(e)
        return False
