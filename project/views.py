# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
import json
from django.http import JsonResponse
from .workHolder import loginUser, creatingNewUserAdmin, creatingNewUserClient, creatingNewGenre, logginOutUser, viewingAllGenres, editingGenre, creatingNewMovie, creatingNewMovieBulk, editingMovie, commentingOnMovieClient, viewingMoviesAll, viewMoviesDetails, viewFilteredMovie, searchTheMovie, isTheUserNameUnique, isPassedValuesUnique
from .utility import loggerHandling

logger = loggerHandling('logs/project.log')

##############################################################
##############################################################
##############################################################
################FOR EVALUATION PURPOSE ONLY###################
#####################Author:HARSHVARDHAN######################
#################REQUEST VIEWING HOLDER LAYER#################
##############################################################
##############################################################
##############################################################

#Login Api Interaction Point Begins
def login(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Login in for'+str(request.body))
                response = loginUser(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Create a new user admin Api Interaction Point Begins
def createadmin(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Creating admin for'+str(request.body))
                response = creatingNewUserAdmin(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Create a new user client Api Interaction Point Begins
def createclient(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Creating new client for'+str(request.body))
                response = creatingNewUserClient(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Create a new genre by admin Api Interaction Point Begins
def creategenre(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Creating new genre for'+str(request.body))
                response = creatingNewGenre(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Logging out a user Api Interaction Point Begins
def logout(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Logging out user for'+str(request.body))
                response = logginOutUser(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Viewing all genre by users Api Interaction Point Begins
def viewgenre(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Viewing all genre for'+str(request.body))
                response = viewingAllGenres(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Editing genre by admin Api Interaction Point Begins
def editgenre(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Editing genre for'+str(request.body))
                response = editingGenre(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Creating new single movie by admin Api Interaction Point Begins
def createmovie(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Creating new movie for'+str(request.body))
                response = creatingNewMovie(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Creating movies in bulk by users Api Interaction Point Begins
def createmoviebulk(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Creating new movie bulk for'+str(request.body))
                response = creatingNewMovieBulk(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Editing movie by users Api Interaction Point Begins
def editmovie(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Editing movie for'+str(request.body))
                response = editingMovie(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Commenting on movie by admin Api Interaction Point Begins
def commentmovie(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Commenting on movie for'+str(request.body))
                response = commentingOnMovieClient(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Viewing all movies list by users Api Interaction Point Begins
def viewmovie(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Viewing movie list for'+str(request.body))
                response = viewingMoviesAll(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Viewing movie details by users Api Interaction Point Begins
def moviedetails(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Viewing movie details for'+str(request.body))
                response = viewMoviesDetails(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Viewing movie list by filters users Api Interaction Point Begins
def viewfilters(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Viewing filters movie list for'+str(request.body))
                response = viewFilteredMovie(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Viewing movie list by searching users Api Interaction Point Begins
def search(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Searching movies for'+str(request.body))
                response = searchTheMovie(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Checking if unique username users Api Interaction Point Begins
def uniqueusername(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Checking if the passed username is unique for'+str(request.body))
                response = isTheUserNameUnique(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Checking if provided email or phone is unique for users (admin and client) Api Interaction Point Begins
def uniquecreation(request):
    try:
        if(request.method=="POST"):
            if(request.body):
                logger.info('Checking unique user creation value for'+str(request.body))
                response = isPassedValuesUnique(request)
                return response
            else:
                response = JsonResponse({"Data":"Blank body passed"})
                response.status_code = 404
                return response
        else:
            response = JsonResponse({"Data":"Wrong Method used"})
            response.status_code = 405
            return response
    #expecting AttributeError
    except AttributeError:
        rData = json
        rData = {"Data":"Something went wrong"}
        response = JsonResponse(rData)
        response.status_code = 500
        logger.exception(AttributeError)
        return response
    #expecting TypeError
    except TypeError:
        x = {"Data":"Something went wrong"}
        response = JsonResponse(x)
        response.status_code = 500
        logger.exception(TypeError)
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response
