from django.db import models

# Create your models here.
class loginMaster(models.Model):
    userId = models.AutoField(primary_key=True)
    userName = models.CharField(unique=True, blank=False, null=False, max_length=64)
    password = models.CharField(unique=False, blank=False, null=False, max_length=32)
    userType = models.CharField(max_length=32, blank=False, null=False, unique=False)
    isActive = models.BooleanField()
    def __str__(self):
        return self.name

class adminMaster(models.Model):
    adminId = models.AutoField(primary_key=True)
    userId = models.ForeignKey(loginMaster, on_delete=models.PROTECT)
    adminName = models.CharField(max_length=255, blank=False, null=False, unique=False)
    number = models.CharField(max_length=12, blank=False, null=False, unique=True)
    email = models.CharField(max_length=255, blank=False, null=False, unique=True)
    isActive = models.BooleanField()
    def __str__(self):
        return self.name

class clientMaster(models.Model):
    clientId = models.AutoField(primary_key=True)
    userId = models.ForeignKey(loginMaster, on_delete=models.PROTECT)
    name = models.CharField(max_length=255, blank=False, null=False, unique=False)
    number = models.CharField(max_length=12, blank=False, null=False, unique=True)
    email = models.CharField(max_length=255, blank=False, null=False, unique=True)
    createOn = models.DateTimeField()
    def __str__(self):
        return self.name

class genreMaster(models.Model):
    genreId = models.AutoField(primary_key=True)
    genreName = models.CharField(max_length=255, blank=False, null=False, unique=True)
    isActive = models.BooleanField()
    def __str__(self):
        return self.name

class sessionMaster(models.Model):
    sessionId = models.AutoField(primary_key=True)
    basicAuthenticate = models.CharField(max_length=128, blank=False, null=False, unique=True)
    sessionInfo = models.CharField(max_length=128, blank=False, null=False, unique=True)
    createdOn = models.DateTimeField()
    userId = models.ForeignKey(loginMaster, on_delete=models.PROTECT)
    typeId = models.IntegerField(blank=False, null=False, unique=False)
    userType = models.CharField(max_length=255, blank=False, null=False, unique=False)
    isActive = models.BooleanField()
    def __str__(self):
        return self.name

class movieMaster(models.Model):
    movieId = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255, blank=False, null=False, unique=False)
    director = models.CharField(max_length=255, blank=False, null=False, unique=False)
    genre = models.TextField(blank=False, null=False, unique=False)
    poplularity = models.DecimalField(max_digits=5, decimal_places=2, unique=False, null=False, blank=False)
    imdb = models.DecimalField(max_digits=3, decimal_places=1, unique=False, null=False, blank=False)
    createdOn = models.DateTimeField()
    def __str__(self):
        return self.name

class commentMaster(models.Model):
    commentId = models.AutoField(primary_key=True)
    movieId = models.ForeignKey(movieMaster, on_delete=models.PROTECT)
    commentBy = models.ForeignKey(clientMaster, on_delete=models.PROTECT)
    commentOn = models.DateTimeField()
    commentValue = models.TextField()
    def __str__(self):
        return self.name
