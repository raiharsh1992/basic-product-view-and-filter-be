# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
from datetime import datetime
from django.http import JsonResponse
from .dataClasses import loginReuest, userCreation, sessionInfo, genreHolder, movieHolder, commentHolder, searchAndFilter
from .dataViewHolder import isValidLoginRequest, isValidSession, isValideNewGenreId, isValidGenre, isValidMovieId, isValidNewUserName, isValidNewAdminPhone, isValidNewAdminEmail, isValidNewClientPhone, isValidNewClientEmail
from .dataControl import getSessionInfoObject, isValidNewAdminEntry, isValidNewClientEntry, getAllClientGenreList, getAllAdminGenreList, isValidGenreUpdate, isAllowedMovieCreation, getAllMvoies, getMovieDetails, getFilterValue, getSearchedMovieValues
from .dataManipulator import insertSessionInformation, insertAdminLoginDetails, insertAdminNewPersonalDetails, deletNewLoginInfo, insertClientLoginDetails, insertClientNewPersonalDetails, insertNewGenre, deActivateANewSession, updateTheValuesOfGenre, insertNewMovieInformation, editingMovieInformation, insertNewComment
from .utility import loggerHandling

logger = loggerHandling('logs/project.log')

##############################################################
##############################################################
##############################################################
################FOR EVALUATION PURPOSE ONLY###################
#####################Author:HARSHVARDHAN######################
####################DATA WORK HOLDER LAYER####################
##############################################################
##############################################################
##############################################################

# Manages the login request for validating if the values are correct and then return the session values
def loginUser(request):
    try:
        #reading Json request
        rString = json.loads(request.body)
        loginReuestObject = loginReuest(rString['userName'], rString['password'], rString['userType'])
        #Check if valid username password and usertype exists in the system
        if isValidLoginRequest(loginReuestObject):
            #Create a session object with the provided login details
            sessionInfoObject = getSessionInfoObject(loginReuestObject)
            #Check if the created session object has values or not
            if sessionInfoObject==0:
                response = JsonResponse({"Data":"There is some inconsitency in data processing"})
                response.status_code = 409
                return response
            else:
                #Insert session information in the database, and if success return session info or else return not able to process information in this moment
                if insertSessionInformation(sessionInfoObject):
                    response = JsonResponse({"Data":"Login successful","sessionInfo":{"basicAuth":sessionInfoObject.basicAuth, "sessionKey":sessionInfoObject.sessionKey,"userId":sessionInfoObject.userId,"userType":sessionInfoObject.userType, "typeId":sessionInfoObject.typeId}})
                    response.status_code = 200
                    return response
                else:
                    response = JsonResponse({"Data":"Not able to login in this moment please try again later"})
                    response.status_code = 409
                    return response
        else:
            response = JsonResponse({"Data":"Invalid login request passed"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Validates if the request if from valid admin to create new admin user
def creatingNewUserAdmin(request):
    try:
        basicAuth = request.META['HTTP_BASICAUTHENTICATE']
        sessionKey = request.META['HTTP_SESSIONKEY']
        rString = json.loads(request.body)
        sessionInfoObject = sessionInfo(0, basicAuth, sessionKey, None, rString['userId'], rString['typeId'], rString['userType'], 1)
        if isValidSession(sessionInfoObject):
            if sessionInfoObject.userType=="ADM":
                adminCreationInfo = userCreation(rString['userName'], rString['password'], rString['adminName'], rString['number'], rString['email'], 0)
                checkIfNewAdminAllowed = isValidNewAdminEntry(adminCreationInfo)
                if checkIfNewAdminAllowed==0:
                    userId = insertAdminLoginDetails(adminCreationInfo)
                    if userId>0:
                        adminCreationInfo.userId = userId
                        if insertAdminNewPersonalDetails(adminCreationInfo):
                            response = JsonResponse({"Data":"New admin created successful"})
                            response.status_code = 200
                            return response
                        else:
                            deletNewLoginInfo(userId)
                            response = JsonResponse({"Data":"Issue while creating new admin user"})
                            response.status_code = 409
                            return response
                    else:
                        response = JsonResponse({"Data":"Issue while creating new admin"})
                        response.status_code = 409
                        return response
                else:
                    return checkIfNewAdminAllowed
            else:
                response = JsonResponse({"Data":"Yoou are not authorized to perform the task"})
                response.status_code = 409
                return response
        else:
            response = JsonResponse({"Data":"Not valid session information passed"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Creating new client user, which allows the user to further comment of movies
def creatingNewUserClient(request):
    try:
        rString = json.loads(request.body)
        clientCreationInfo = userCreation(rString['userName'], rString['password'], rString['clientName'], rString['number'], rString['email'], 0)
        checkIfNewClientAllowed = isValidNewClientEntry(clientCreationInfo)
        if checkIfNewClientAllowed==0:
            userId = insertClientLoginDetails(clientCreationInfo)
            if userId>0:
                clientCreationInfo.userId = userId
                if insertClientNewPersonalDetails(clientCreationInfo):
                    loginReuestObject = loginReuest(clientCreationInfo.userName, clientCreationInfo.password, "CLIENT")
                    sessionInfoObject = getSessionInfoObject(loginReuestObject)
                    if sessionInfoObject==0:
                        response = JsonResponse({"Data":"There is some inconsitency in data processing"})
                        response.status_code = 409
                        return response
                    else:
                        #Insert session information in the database, and if success return session info or else return not able to process information in this moment
                        if insertSessionInformation(sessionInfoObject):
                            response = JsonResponse({"Data":"CLient user created, Login successful","sessionInfo":{"basicAuth":sessionInfoObject.basicAuth, "sessionKey":sessionInfoObject.sessionKey,"userId":sessionInfoObject.userId,"userType":sessionInfoObject.userType, "typeId":sessionInfoObject.typeId}})
                            response.status_code = 200
                            return response
                        else:
                            response = JsonResponse({"Data":"Not able to login in this moment please try again later"})
                            response.status_code = 409
                            return response
                else:
                    deletNewLoginInfo(userId)
                    response = JsonResponse({"Data":"Issue while creating new user"})
                    response.status_code = 409
                    return response
            else:
                response = JsonResponse({"Data":"Issue while creating new user"})
                response.status_code = 409
                return response
        else:
            return checkIfNewClientAllowed
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#The admin validation, followed by creation of new genre in the system if the provided name is unique
def creatingNewGenre(request):
    try:
        basicAuth = request.META['HTTP_BASICAUTHENTICATE']
        sessionKey = request.META['HTTP_SESSIONKEY']
        rString = json.loads(request.body)
        sessionInfoObject = sessionInfo(0, basicAuth, sessionKey, None, rString['userId'], rString['typeId'], rString['userType'], 1)
        if isValidSession(sessionInfoObject):
            if sessionInfoObject.userType=="ADM":
                genreClassObject = genreHolder(0, rString['name'], 1)
                if isValideNewGenreId(genreClassObject.name):
                    if insertNewGenre(genreClassObject):
                        response = JsonResponse({"Data":"New genre created successful"})
                        response.status_code = 200
                        return response
                    else:
                        response = JsonResponse({"Data":"Issue while creating new genre"})
                        response.status_code = 409
                        return response
                else:
                    response = JsonResponse({"Data":"Genre with the name already exists"})
                    response.status_code = 409
                    return response
            else:
                response = JsonResponse({"Data":"Yoou are not authorized to perform the task"})
                response.status_code = 409
                return response
        else:
            response = JsonResponse({"Data":"Not valid session information passed"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Logging out a user, by deactivating the session of the user
def logginOutUser(request):
    try:
        basicAuth = request.META['HTTP_BASICAUTHENTICATE']
        sessionKey = request.META['HTTP_SESSIONKEY']
        rString = json.loads(request.body)
        sessionInfoObject = sessionInfo(0, basicAuth, sessionKey, None, rString['userId'], rString['typeId'], rString['userType'], 1)
        if isValidSession(sessionInfoObject):
            if deActivateANewSession(sessionInfoObject):
                response = JsonResponse({"Data":"User logged out scuccesfuly"})
                response.status_code = 200
                return response
            else:
                response = JsonResponse({"Data":"Issue while logging out user"})
                response.status_code = 409
                return response
        else:
            response = JsonResponse({"Data":"Not valid session information passed"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Viewing all the genres both active and inactive and values are returned as per the request, if the request if for client no session is needed and only active genres are sent, if admin then session is validated and then the genre(both) are sent
def viewingAllGenres(request):
    try:
        rString = json.loads(request.body)
        viewType = rString['viewType']
        if viewType=="CLIENT":
            response = getAllClientGenreList()
            return response
        elif viewType=="ADM":
            basicAuth = request.META['HTTP_BASICAUTHENTICATE']
            sessionKey = request.META['HTTP_SESSIONKEY']
            sessionInfoObject = sessionInfo(0, basicAuth, sessionKey, None, rString['userId'], rString['typeId'], rString['userType'], 1)
            if isValidSession(sessionInfoObject):
                if sessionInfoObject.userType=="ADM":
                    response = getAllAdminGenreList()
                    return response
                else:
                    response = JsonResponse({"Data":"Invalid session info for admin passed"})
                    response.status_code = 409
                    return response
            else:
                response = JsonResponse({"Data":"Not valid session information passed"})
                response.status_code = 401
                return response
        else:
            response = JsonResponse({"Data":"Invalid view type selected"})
            response.status_code = 409
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Editing genre status, name by admin. Can be performed only adter validating admin session
def editingGenre(request):
    try:
        basicAuth = request.META['HTTP_BASICAUTHENTICATE']
        sessionKey = request.META['HTTP_SESSIONKEY']
        rString = json.loads(request.body)
        sessionInfoObject = sessionInfo(0, basicAuth, sessionKey, None, rString['userId'], rString['typeId'], rString['userType'], 1)
        if isValidSession(sessionInfoObject):
            if sessionInfoObject.userType=="ADM":
                genreClassObject = genreHolder(rString['genreId'], rString['name'], rString['isActive'])
                if isValidGenre(genreClassObject.genreId):
                    if isValidGenreUpdate(genreClassObject):
                        if updateTheValuesOfGenre(genreClassObject):
                            response = JsonResponse({"Data":"Genre updated scuccesfuly"})
                            response.status_code = 200
                            return response
                        else:
                            response = JsonResponse({"Data":"Issue occured while updating genre"})
                            response.status_code = 409
                            return response
                    else:
                        response = JsonResponse({"Data":"A genre with provided updated name already exist"})
                        response.status_code = 409
                        return response
                else:
                    response = JsonResponse({"Data":"Invalid genre selected"})
                    response.status_code = 409
                    return response
            else:
                response = JsonResponse({"Data":"You are not authorized"})
                response.status_code = 409
                return response
        else:
            response = JsonResponse({"Data":"Not valid session information passed"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Creating new movie by admin, we check for admin session, then we check for genre creation that is if the genre name is present if not we create a new genre and then insert the movie information
def creatingNewMovie(request):
    try:
        basicAuth = request.META['HTTP_BASICAUTHENTICATE']
        sessionKey = request.META['HTTP_SESSIONKEY']
        rString = json.loads(request.body)
        sessionInfoObject = sessionInfo(0, basicAuth, sessionKey, None, rString['userId'], rString['typeId'], rString['userType'], 1)
        if isValidSession(sessionInfoObject):
            if sessionInfoObject.userType=="ADM":
                createdOnDate =  datetime.now()
                movieClassObject = movieHolder(0, rString['name'], rString['director'], rString['genre'], createdOnDate, rString['poplularity'], rString['imdb'])
                if isAllowedMovieCreation(movieClassObject):
                    if insertNewMovieInformation(movieClassObject):
                        response = JsonResponse({"Data":"New movie information created scuccesfuly"})
                        response.status_code = 200
                        return response
                    else:
                        response = JsonResponse({"Data":"Issue while creating new movie"})
                        response.status_code = 409
                        return response
                else:
                    response = JsonResponse({"Data":"Provided information is not sufficient to create a new movie"})
                    response.status_code = 409
                    return response
            else:
                response = JsonResponse({"Data":"You are not authorized"})
                response.status_code = 409
                return response
        else:
            response = JsonResponse({"Data":"Not valid session information passed"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Used for bulk insertion of movie, can be performed by admin by inserting json array of the request shared
def creatingNewMovieBulk(request):
    try:
        basicAuth = request.META['HTTP_BASICAUTHENTICATE']
        sessionKey = request.META['HTTP_SESSIONKEY']
        rString = json.loads(request.body)
        sessionInfoObject = sessionInfo(0, basicAuth, sessionKey, None, rString['userId'], rString['typeId'], rString['userType'], 1)
        if isValidSession(sessionInfoObject):
            if sessionInfoObject.userType=="ADM":
                createdOnDate =  datetime.now()
                requestObject = rString['requestObject']
                totalSuccess = 0
                totalFailure = 0
                for each in requestObject:
                    movieClassObject = movieHolder(0, each['name'], each['director'], each['genre'], createdOnDate, each['99popularity'], each['imdb_score'])
                    if isAllowedMovieCreation(movieClassObject):
                        if insertNewMovieInformation(movieClassObject):
                            totalSuccess = totalSuccess+1
                        else:
                            totalFailure+1
                    else:
                        totalFailure+1
                response = JsonResponse({"Data":"Bulk upload creation completed","totalSuccess":totalSuccess,"totalFailure":totalFailure})
                response.status_code = 200
                return response
            else:
                response = JsonResponse({"Data":"You are not authorized"})
                response.status_code = 409
                return response
        else:
            response = JsonResponse({"Data":"Not valid session information passed"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Editing informations of the movie and can be done only by the admin
def editingMovie(request):
    try:
        basicAuth = request.META['HTTP_BASICAUTHENTICATE']
        sessionKey = request.META['HTTP_SESSIONKEY']
        rString = json.loads(request.body)
        sessionInfoObject = sessionInfo(0, basicAuth, sessionKey, None, rString['userId'], rString['typeId'], rString['userType'], 1)
        if isValidSession(sessionInfoObject):
            if sessionInfoObject.userType=="ADM":
                createdOnDate =  datetime.now()
                movieClassObject = movieHolder(rString['movieId'], rString['name'], rString['director'], rString['genre'], createdOnDate, rString['poplularity'], rString['imdb'])
                if isValidMovieId(movieClassObject.id):
                    if isAllowedMovieCreation(movieClassObject):
                        if editingMovieInformation(movieClassObject):
                            response = JsonResponse({"Data":"Movie information edited scuccesfuly"})
                            response.status_code = 200
                            return response
                        else:
                            response = JsonResponse({"Data":"Issue while editing movie"})
                            response.status_code = 409
                            return response
                    else:
                        response = JsonResponse({"Data":"Provided information is not sufficient to edit movie"})
                        response.status_code = 409
                        return response
                else:
                    response = JsonResponse({"Data":"Invalid movie selected for editing"})
                    response.status_code = 409
                    return response
            else:
                response = JsonResponse({"Data":"You are not authorized"})
                response.status_code = 409
                return response
        else:
            response = JsonResponse({"Data":"Not valid session information passed"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#Here we check if the session if of client and then allow comments to be inserted to valid movie informations
def commentingOnMovieClient(request):
    try:
        basicAuth = request.META['HTTP_BASICAUTHENTICATE']
        sessionKey = request.META['HTTP_SESSIONKEY']
        rString = json.loads(request.body)
        sessionInfoObject = sessionInfo(0, basicAuth, sessionKey, None, rString['userId'], rString['typeId'], rString['userType'], 1)
        if isValidSession(sessionInfoObject):
            if sessionInfoObject.userType=="CLIENT":
                createdOnDate =  datetime.now()
                commentClassObject = commentHolder(rString['movieId'], sessionInfoObject.typeId, createdOnDate, rString['comment'])
                if isValidMovieId(commentClassObject.movieId):
                    if insertNewComment(commentClassObject):
                        response = JsonResponse({"Data":"Comment inserted scuccesfuly"})
                        response.status_code = 200
                        return response
                    else:
                        response = JsonResponse({"Data":"Issue while inserting new comment"})
                        response.status_code = 409
                        return response
                else:
                    response = JsonResponse({"Data":"Invalid movie selcted for commenting"})
                    response.status_code = 409
                    return response
            else:
                response = JsonResponse({"Data":"You are not authorized"})
                response.status_code = 409
                return response
        else:
            response = JsonResponse({"Data":"Not valid session information passed"})
            response.status_code = 401
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#This is a public api, where we get the request size and page count and then return
def viewingMoviesAll(request):
    try:
        rString = json.loads(request.body)
        response = getAllMvoies(rString['totalCount'], rString['pageCount'])
        return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#This is a public api, where we return movie details
def viewMoviesDetails(request):
    try:
        rString = json.loads(request.body)
        movieId = rString['movieId']
        if isValidMovieId(movieId):
            response = getMovieDetails(movieId)
            return response
        else:
            response = JsonResponse({"Data":"Invalid movie selected for viewing"})
            response.status_code = 409
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#This is a public api, where we get the request size and page count and then return and the requested genre
def viewFilteredMovie(request):
    try:
        rString = json.loads(request.body)
        searchAndFilterObject = searchAndFilter(rString['searchValue'], rString['pageCount'], rString['totalViewCount'])
        if isValideNewGenreId(searchAndFilterObject.searchValue):
            response = JsonResponse({"Data":"Invalid filter parameter passed"})
            response.status_code = 409
            return response
        else:
            if searchAndFilterObject.pageCount>0:
                if searchAndFilterObject.totalViewCount>0 and searchAndFilterObject.totalViewCount<501:
                    response = getFilterValue(searchAndFilterObject)
                    return response
                else:
                    response = JsonResponse({"Data":"Total count should be more than 0 and less than 501"})
                    response.status_code = 409
                    return response
            else:
                response = JsonResponse({"Data":"Page count should be more than 0"})
                response.status_code = 409
                return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#This is a public api, where we get the request size and page count and then return. Here we get the search parameter
def searchTheMovie(request):
    try:
        rString = json.loads(request.body)
        searchAndFilterObject = searchAndFilter(rString['searchValue'], rString['pageCount'], rString['totalViewCount'])
        if searchAndFilterObject.pageCount>0:
            if searchAndFilterObject.totalViewCount>0 and searchAndFilterObject.totalViewCount<501:
                response = getSearchedMovieValues(searchAndFilterObject)
                return response
            else:
                response = JsonResponse({"Data":"Total count should be more than 0 and less than 501"})
                response.status_code = 409
                return response
        else:
            response = JsonResponse({"Data":"Page count should be more than 0"})
            response.status_code = 409
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#This is a public api, where we get the request username and then validate if it is unique or not
def isTheUserNameUnique(request):
    try:
        rString = json.loads(request.body)
        userName = rString['userName']
        if isValidNewUserName(userName):
            response = JsonResponse({"Data":"Username is available"})
            response.status_code = 200
            return response
        else:
            response = JsonResponse({"Data":"Username is already taken"})
            response.status_code = 409
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response

#This is a public api, where we get the request to check if the provided email or phone for admin or client is unique or not and we respond accordingly
def isPassedValuesUnique(request):
    try:
        rString = json.loads(request.body)
        requestType = rString['requestType']
        requestValue = rString['requestValue']
        requestFor = rString['requestFor']
        if requestFor=="ADM":
            if requestType=="PHONE":
                if isValidNewAdminPhone(requestValue):
                    response = JsonResponse({"Data":"Unique phone number passed"})
                    response.status_code = 200
                    return response
                else:
                    response = JsonResponse({"Data":"Phone number already taken"})
                    response.status_code = 409
                    return response
            elif requestType=="EMAIL":
                if isValidNewAdminEmail(requestValue):
                    response = JsonResponse({"Data":"Unique email address passed"})
                    response.status_code = 200
                    return response
                else:
                    response = JsonResponse({"Data":"Email address already taken"})
                    response.status_code = 409
                    return response
            else:
                response = JsonResponse({"Data":"Invalid request type selected"})
                response.status_code = 409
                return response
        elif requestFor=="CLIENT":
            if requestType=="PHONE":
                if isValidNewClientPhone(requestValue):
                    response = JsonResponse({"Data":"Unique phone number passed"})
                    response.status_code = 200
                    return response
                else:
                    response = JsonResponse({"Data":"Phone number already taken"})
                    response.status_code = 409
                    return response
            elif requestType=="EMAIL":
                if isValidNewClientEmail(requestValue):
                    response = JsonResponse({"Data":"Unique email address passed"})
                    response.status_code = 200
                    return response
                else:
                    response = JsonResponse({"Data":"Email address already taken"})
                    response.status_code = 409
                    return response
            else:
                response = JsonResponse({"Data":"Invalid request type selected"})
                response.status_code = 409
                return response
        else:
            response = JsonResponse({"Data":"Requesting for invalid user type"})
            response.status_code = 409
            return response
    #expecting TypeError
    except KeyError :
        rData = json
        rData = {"Data":"Not all variables are passed"}
        response = JsonResponse(rData)
        response.status_code = 404
        logger.exception(KeyError)
        return response
    #check when a value is passed with a variable
    except ValueError :
        x = json
        x = {"Data":"Not all variable values passed"}
        response = JsonResponse(x)
        response.status_code = 404
        logger.exception(ValueError)
        return response
