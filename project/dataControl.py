# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
from django.http import JsonResponse
import uuid
from datetime import datetime
from .dataViewHolder import getUserIdFromLoginInfo, getAdminIdFromuserId, getClientIdFromUserId, validSessionBasicAut, validSessionSessionKey, isValidNewUserName, isValidNewAdminPhone, isValidNewAdminEmail, isValidNewClientPhone, isValidNewClientEmail, getGenreListActive, getGenreListAll, isValideNewGenreId, getGenreIdFromName, isValideNewGenreId, getMovieListData, getTotalMovieCount, getMoviComments, getClientNameByClientId, getSelectedMovieInfo, getPagedFilteredObject, getFilteredPageCount, getSearchedPageCount, getPagedSearchedObject
from .dataClasses import sessionInfo, genreHolder
from .dataManipulator import insertNewGenre
from .utility import isValidNewPassword

##############################################################
##############################################################
##############################################################
################FOR EVALUATION PURPOSE ONLY###################
#####################Author:HARSHVARDHAN######################
##################DATA CONTROL HOLDER LAYER###################
##############################################################
##############################################################
##############################################################

def getSessionInfoObject(loginRequest):
    userId = getUserIdFromLoginInfo(loginRequest)
    if userId ==0:
        return userId
    else:
        typeId = 0
        if loginRequest.userType=="ADM":
            typeId = getAdminIdFromuserId(userId)
        elif loginRequest.userType=="CLIENT":
            typeId = getClientIdFromUserId(userId)
        else:
            return 0
        if typeId ==0:
            return typeId
        else:
            basicKey = uuid.uuid4().hex
            if validSessionBasicAut(basicKey)!=0:
                while validSessionBasicAut(basicKey)!=0:
                    basicKey = uuid.uuid4().hex
            sessionKey = uuid.uuid4().hex
            if validSessionSessionKey(sessionKey)!=0:
                while validSessionSessionKey(sessionKey)!=0:
                    sessionKey = uuid.uuid4().hex
            createdOnDate =  datetime.now()
            sessionInformation = sessionInfo(0, basicKey, sessionKey, createdOnDate, userId, typeId, loginRequest.userType, 1)
            return sessionInformation

def isValidNewAdminEntry(adminCreationInfo):
    if isValidNewUserName(adminCreationInfo.userName):
        if isValidNewPassword(adminCreationInfo.password):
            if isValidNewAdminPhone(adminCreationInfo.number):
                if isValidNewAdminEmail(adminCreationInfo.email):
                    return 0
                else:
                    response = JsonResponse({"Data":"Admin with the email already exists"})
                    response.status_code = 409
                    return response
            else:
                response = JsonResponse({"Data":"Admin user with the number already exists"})
                response.status_code = 409
                return response
        else:
            response = JsonResponse({"Data":"Invalid password, please try new"})
            response.status_code = 409
            return response
    else:
        response = JsonResponse({"Data":"Invalid Username, Username already taken"})
        response.status_code = 409
        return response

def isValidNewClientEntry(adminCreationInfo):
    if isValidNewUserName(adminCreationInfo.userName):
        if isValidNewPassword(adminCreationInfo.password):
            if isValidNewClientPhone(adminCreationInfo.number):
                if isValidNewClientEmail(adminCreationInfo.email):
                    return 0
                else:
                    response = JsonResponse({"Data":"Client user with the email already exists"})
                    response.status_code = 409
                    return response
            else:
                response = JsonResponse({"Data":"Client user with the number already exists"})
                response.status_code = 409
                return response
        else:
            response = JsonResponse({"Data":"Invalid password, please try new"})
            response.status_code = 409
            return response
    else:
        response = JsonResponse({"Data":"Invalid Username, Username already taken"})
        response.status_code = 409
        return response

def getAllClientGenreList():
    genreList = getGenreListActive()
    container = []
    totalCount = 0
    for each in genreList:
        container.append({
            "genreId":each[0],
            "genreName":each[1]
        })
        totalCount = totalCount+1
    response = JsonResponse({"Data":container,"totalCount":totalCount})
    response.status_code = 200
    return response

def getAllAdminGenreList():
    genreList = getGenreListAll()
    container = []
    totalCount = 0
    for each in genreList:
        container.append({
            "genreId":each[0],
            "genreName":each[1],
            "isActive":each[2]
        })
        totalCount = totalCount+1
    response = JsonResponse({"Data":container,"totalCount":totalCount})
    response.status_code = 200
    return response

def isValidGenreUpdate(genreClassObject):
    if isValideNewGenreId(genreClassObject.name):
        return True
    else:
        genreId = getGenreIdFromName(genreClassObject.name)
        if genreId == genreClassObject.genreId:
            return True
        else:
            return False

def isAllowedMovieCreation(movieClassObject):
    if movieClassObject.poplularity>100:
        return False
    if movieClassObject.imdb>10:
        return False
    for each in movieClassObject.genre:
        if isValideNewGenreId(each):
            genreClassObject = genreHolder(0, each, 1)
            insertNewGenre(genreClassObject)
    return True

def getAllMvoies(totalCount, pageCount):
    if totalCount>0 and totalCount<501:
        if pageCount>0:
            pageCount = pageCount-1
            movieListData = getMovieListData(totalCount, pageCount)
            totalMovieCount = getTotalMovieCount()
            container = []
            for each in movieListData:
                container.append({
                    "movieId":each[0],
                    "name":each[1],
                    "popularity":each[4],
                    "imdb":each[5],
                    "director":each[2]
                })
            response = JsonResponse({"Data":container,"totalMovieCount":totalMovieCount})
            response.status_code = 200
            return response
        else:
            response = JsonResponse({"Data":"Page count shoule be more than 0"})
            response.status_code = 409
            return response
    else:
        response = JsonResponse({"Data":"Page count should be more than 0 or less than 501"})
        response.status_code = 409
        return response

def getMovieDetails(movieId):
    commentInfo = getMoviComments(movieId)
    totalComments = 0
    container = []
    for each in commentInfo:
        commentBy = getClientNameByClientId(each[2])
        container.append({
            "commentBy":commentBy,
            "commentOn":each[3],
            "comment":each[4]
        })
        totalComments = totalComments+1
    movieInfo = getSelectedMovieInfo(movieId)
    genre = movieInfo[0][3]
    genre = genre.strip('][').split(', ')
    print(genre)
    response = JsonResponse({"Data":{"name":movieInfo[0][1],"director":movieInfo[0][2],"genre":genre,"popularity":movieInfo[0][4],"imdb":movieInfo[0][5],"createdOn":movieInfo[0][6],"comments":container}})
    response.status_code = 200
    return response

def getFilterValue(searchAndFilterObject):
    searchAndFilterObject.pageCount = searchAndFilterObject.pageCount-1
    filteredObject = getPagedFilteredObject(searchAndFilterObject)
    totalFilteredObject = getFilteredPageCount(searchAndFilterObject)
    container = []
    for each in filteredObject:
        container.append({
            "movieId":each[0],
            "name":each[1],
            "popularity":each[4],
            "imdb":each[5],
            "director":each[2]
        })
    response = JsonResponse({"Data":container,"totalMovieCount":totalFilteredObject})
    response.status_code = 200
    return response

def getSearchedMovieValues(searchAndFilterObject):
    searchAndFilterObject.pageCount = searchAndFilterObject.pageCount-1
    filteredObject = getPagedSearchedObject(searchAndFilterObject)
    totalFilteredObject = getSearchedPageCount(searchAndFilterObject)
    container = []
    for each in filteredObject:
        container.append({
            "movieId":each[0],
            "name":each[1],
            "popularity":each[4],
            "imdb":each[5],
            "director":each[2]
        })
    response = JsonResponse({"Data":container,"totalMovieCount":totalFilteredObject})
    response.status_code = 200
    return response
