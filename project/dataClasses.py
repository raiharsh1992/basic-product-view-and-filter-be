# -*- coding: utf-8 -*-
##############################################################
##############################################################
##############################################################
################FOR EVALUATION PURPOSE ONLY###################
#####################Author:HARSHVARDHAN######################
#################DATA CLASS AND OBJECT LAYER##################
##############################################################
##############################################################
##############################################################
class loginReuest:
    def __init__(self, userName, password, userType):
        self.userName = userName
        self.password = password
        self.userType = userType

class sessionInfo:
    def __init__(self, sessionId, basicAuth, sessionKey, createdOn, userId, typeId, userType, isActive):
        self.sessionId = sessionId
        self.basicAuth = basicAuth
        self.sessionKey = sessionKey
        self.createdOn = createdOn
        self.userId = userId
        self.typeId = typeId
        self.userType = userType
        self.isActive = isActive

class userCreation:
    def __init__(self, userName, password, adminName, number, email, userId):
        self.userName = userName
        self.password = password
        self.adminName = adminName
        self.number = number
        self.email = email
        self.userId = userId

class genreHolder:
    def __init__(self, genreId, name, isActive):
        self.genreId = genreId
        self.name = name
        self.isActive = isActive

class movieHolder:
    def __init__(self, id, name, director, genre, createdOn, poplularity, imdb):
        self.id = id
        self.name = name
        self.director = director
        self.genre = genre
        self.createdOn = createdOn
        self.poplularity = poplularity
        self.imdb = imdb

class commentHolder:
    def __init__(self, movieId, clientId, createdOn, comment):
        self.movieId = movieId
        self.clientId = clientId
        self.createdOn = createdOn
        self.comment = comment

class searchAndFilter:
    def __init__(self, searchValue, pageCount, totalViewCount):
        self.searchValue = searchValue
        self.pageCount = pageCount
        self.totalViewCount = totalViewCount
