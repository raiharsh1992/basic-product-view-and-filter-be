# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
from .models import loginMaster, adminMaster, sessionMaster, clientMaster, genreMaster, movieMaster, commentMaster
from django.db.models import Q

##############################################################
##############################################################
##############################################################
################FOR EVALUATION PURPOSE ONLY###################
#####################Author:HARSHVARDHAN######################
##################DATA VIEWING HOLDER LAYER###################
##############################################################
##############################################################
##############################################################

def isValidLoginRequest(loginRequest):
    #Validating if one and only one record with provide username, password and usertype exist in login master
    loginInfo = loginMaster.objects.values_list().filter(userName__exact=loginRequest.userName).filter(password__exact=loginRequest.password).filter(userType__exact=loginRequest.userType).filter(isActive=1).count()
    if loginInfo==1:
        return True
    else:
        return False

def getUserIdFromLoginInfo(loginRequest):
    loginInfo = loginMaster.objects.values_list().filter(userName__exact=loginRequest.userName).filter(password__exact=loginRequest.password).filter(userType__exact=loginRequest.userType).filter(isActive=1)
    return loginInfo[0][0]

def getAdminIdFromuserId(userId):
    adminInfoCount = adminMaster.objects.values_list().filter(userId=userId).count()
    if adminInfoCount>0:
        adminInfo = adminMaster.objects.values_list().filter(userId=userId)
        return adminInfo[0][0]
    else:
        return 0

def getClientIdFromUserId(userId):
    clientInfoCount = clientMaster.objects.values_list().filter(userId=userId).count()
    if clientInfoCount>0:
        clientInfo = clientMaster.objects.values_list().filter(userId=userId)
        return clientInfo[0][0]
    else:
        return 0

def validSessionBasicAut(basicKey):
    sessionInformation = sessionMaster.objects.values_list().filter(basicAuthenticate__exact=basicKey).count()
    return sessionInformation

def validSessionSessionKey(sessionKey):
    sessionInformation = sessionMaster.objects.values_list().filter(sessionInfo__exact=sessionKey).count()
    return sessionInformation

def getLoginObject(userId):
    loginObject = loginMaster.objects.get(userId=userId)
    return loginObject

def isValidSession(sessionInfoObject):
    sessionInformation = sessionMaster.objects.values_list().filter(sessionInfo__exact=sessionInfoObject.sessionKey).filter(basicAuthenticate__exact=sessionInfoObject.basicAuth).filter(userType__exact=sessionInfoObject.userType).filter(userId=sessionInfoObject.userId).filter(typeId=sessionInfoObject.typeId).filter(isActive=1).count()
    if sessionInformation==1:
        return True
    else:
        return False

def isValidNewUserName(userName):
    loginInfo = loginMaster.objects.values_list().filter(userName__exact=userName).count()
    if loginInfo==0:
        return True
    else:
        return False

def isValidNewAdminPhone(number):
    adminInfoCount = adminMaster.objects.values_list().filter(number__exact=number).count()
    if adminInfoCount==0:
        return True
    else:
        return False

def isValidNewAdminEmail(email):
    adminInfoCount = adminMaster.objects.values_list().filter(email__exact=email).count()
    if adminInfoCount==0:
        return True
    else:
        return False

def isValidNewClientPhone(number):
    clientInfoCount = clientMaster.objects.values_list().filter(number__exact=number).count()
    if clientInfoCount==0:
        return True
    else:
        return False

def isValidNewClientEmail(email):
    clientInfoCount = clientMaster.objects.values_list().filter(email__exact=email).count()
    if clientInfoCount==0:
        return True
    else:
        return False

def isValideNewGenreId(name):
    genreCount = genreMaster.objects.values_list().filter(genreName__exact=name).count()
    if genreCount==0:
        return True
    else:
        return False

def getGenreListActive():
    genreInfo = genreMaster.objects.values_list().filter(isActive=1)
    return genreInfo

def getGenreListAll():
    genreInfo = genreMaster.objects.values_list()
    return genreInfo

def isValidGenre(genreId):
    genreInfoCount = genreMaster.objects.values_list().filter(genreId=genreId).count()
    if genreInfoCount==1:
        return True
    else:
        return False

def getGenreIdFromName(genreName):
    genreInfo = genreMaster.objects.values_list().filter(genreName__exact=genreName)
    return genreInfo[0][0]

def isValidMovieId(movieId):
    movieInfoCount = movieMaster.objects.values_list().filter(movieId=movieId).count()
    if movieInfoCount==1:
        return True
    else:
        return False

def getMovieObject(movieId):
    movieInfo = movieMaster.objects.get(movieId=movieId)
    return movieInfo

def getClientObject(clientId):
    clientInfo = clientMaster.objects.get(clientId=clientId)
    return clientInfo

def getTotalMovieCount():
    movieInfo = movieMaster.objects.values_list().count()
    return movieInfo

def getMovieListData(totalCount, pageCount):
    offset = totalCount*pageCount
    movieInfo = movieMaster.objects.values_list().order_by('-movieId')[offset:offset+totalCount]
    return movieInfo

def getMoviComments(movieId):
    commentInfo = commentMaster.objects.values_list().filter(movieId=movieId)
    return commentInfo

def getClientNameByClientId(clientId):
    clientInfo = clientMaster.objects.values_list().filter(clientId=clientId)
    return clientInfo[0][2]

def getSelectedMovieInfo(movieId):
    movieInfo = movieMaster.objects.values_list().filter(movieId=movieId)
    return movieInfo

def getFilteredPageCount(searchAndFilterObject):
    movieInfo = movieMaster.objects.values_list().filter(genre__contains=searchAndFilterObject.searchValue).count()
    return movieInfo

def getPagedFilteredObject(searchAndFilterObject):
    offset = searchAndFilterObject.pageCount*searchAndFilterObject.totalViewCount
    movieInfo = movieMaster.objects.values_list().filter(genre__contains=searchAndFilterObject.searchValue)[offset:offset+searchAndFilterObject.totalViewCount]
    return movieInfo

def getSearchedPageCount(searchAndFilterObject):
    movieInfo = movieMaster.objects.values_list().filter(Q(name__contains=searchAndFilterObject.searchValue)|Q(director__contains=searchAndFilterObject.searchValue)|Q(genre__contains=searchAndFilterObject.searchValue)).count()
    return movieInfo

def getPagedSearchedObject(searchAndFilterObject):
    offset = searchAndFilterObject.pageCount*searchAndFilterObject.totalViewCount
    movieInfo = movieMaster.objects.values_list().filter(Q(name__contains=searchAndFilterObject.searchValue)|Q(director__contains=searchAndFilterObject.searchValue)|Q(genre__contains=searchAndFilterObject.searchValue))[offset:offset+searchAndFilterObject.totalViewCount]
    return movieInfo
