# Generated by Django 3.0.7 on 2020-07-28 11:50

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='genreMaster',
            fields=[
                ('genreId', models.AutoField(primary_key=True, serialize=False)),
                ('genreName', models.CharField(max_length=255, unique=True)),
                ('isActive', models.BooleanField()),
            ],
        ),
        migrations.CreateModel(
            name='movieMaster',
            fields=[
                ('movieId', models.AutoField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=255)),
                ('director', models.CharField(max_length=255)),
                ('genre', models.TextField()),
                ('poplularity', models.DecimalField(decimal_places=2, max_digits=3)),
                ('imdb', models.DecimalField(decimal_places=1, max_digits=2)),
                ('createdOn', models.DateTimeField()),
            ],
        ),
        migrations.AlterField(
            model_name='loginmaster',
            name='userId',
            field=models.AutoField(primary_key=True, serialize=False),
        ),
        migrations.CreateModel(
            name='sessionMaster',
            fields=[
                ('sessionId', models.AutoField(primary_key=True, serialize=False)),
                ('basicAuthenticate', models.CharField(max_length=128, unique=True)),
                ('sessionInfo', models.CharField(max_length=128, unique=True)),
                ('createdOn', models.DateTimeField()),
                ('typeId', models.IntegerField()),
                ('userType', models.CharField(max_length=255)),
                ('isActive', models.BooleanField()),
                ('userId', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='project.loginMaster')),
            ],
        ),
        migrations.CreateModel(
            name='clientMaster',
            fields=[
                ('clientId', models.AutoField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=255)),
                ('number', models.CharField(max_length=12, unique=True)),
                ('email', models.CharField(max_length=255, unique=True)),
                ('createOn', models.DateTimeField()),
                ('userId', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='project.loginMaster')),
            ],
        ),
        migrations.CreateModel(
            name='adminMaster',
            fields=[
                ('adminId', models.AutoField(primary_key=True, serialize=False)),
                ('adminName', models.CharField(max_length=255)),
                ('number', models.CharField(max_length=12, unique=True)),
                ('email', models.CharField(max_length=255, unique=True)),
                ('isActive', models.BooleanField()),
                ('userId', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='project.loginMaster')),
            ],
        ),
    ]
